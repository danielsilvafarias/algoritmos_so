quantum = 4
troca_contexto = 2
tempo_decorrido = 0

lista_processos = [5,10,12]
lista_indexada = [] #Lista temporaria para subtracoes
lista_final = [] #Lista final com tempos de turnaround

for i in range(len(lista_processos)):
	lista_indexada.append([i, lista_processos[i]])

#Lista Circular Funcionando:
while(len(lista_indexada) > 0): #Enquanto Houver processos na lista;
	index = 0
	while(index < len(lista_indexada)):
		if(lista_indexada[index][1] <= quantum):
			tempo_decorrido += lista_indexada[index][1]
			antigo = lista_indexada.pop(index)
			lista_final.append([antigo[0], tempo_decorrido])
			index -= 1 #Ao retirar um elemento necessita voltar um indice;
		else:	
			tempo_decorrido += quantum
			lista_indexada[index][1] -= quantum		
		#Incrementando a Troca de Contexto:
		index += 1

		if(len(lista_indexada) > 1):
			tempo_decorrido += troca_contexto

#Exibindo os processos:
lista_final.sort()

#print lista_final
#Exibindo Tempo de Execucao:
print "Tempo de Execucao"
for i in range(len(lista_processos)):
	print "p%d: %d" % (i+1, lista_processos[i])

#Exibindo Tempo de TurnAround:
print "Tempo de TurnAround"
for i in range(len(lista_final)):
	print "p%d: %d" % (i+1, lista_final[i][1])

#Exibindo Tempo de Espera:
print "Tempo de Espera"
for i in range(len(lista_final)):
	print "p%d: %d" % (i+1, (lista_final[i][1] - lista_processos[i]))

#Tempos Medios:
somatorio = 0
print "Tempo Medio de TurnAround"
for i in range(len(lista_final)):
	somatorio += lista_final[i][1]
print "Valor: %.2f" % (float(somatorio) / len(lista_final))

#Exibindo Tempo de Espera:
somatorio = 0
print "Tempo Medio de Espera"
for i in range(len(lista_final)):
	somatorio += lista_final[i][1] - lista_processos[i]
print "Valor: %.2f" % (float(somatorio) / len(lista_final))