# -*- encode : utf-8 -*-
class Processo():
	contador = 0
	
	def __init__(self, tamanho):
		self.id = self.contaUm()
		self.tamanho = tamanho
		self.tempo_espera = 0
		self.tempo_turnaround = 0

	def contaUm(self):
		Processo.contador += 1
		return Processo.contador

	@staticmethod
	def resetCount():
		Processo.contador = 0

	def getId(self):
		return self.id

	def setId(self, id):
		self.id = id

	def getTamanho(self):
		return self.tamanho

	def setTamanho(self, tamanho):
		self.tamanho = tamanho

	def getTempoEspera(self):
		return self.tempo_espera

	def setTempoEspera(self, tempo_espera):
		self.tempo_espera = tempo_espera

	def getTempoTurnaround(self):
		return self.tempo_turnaround

	def setTempoTurnaround(self, tempo_turnaround):
		self.tempo_turnaround = tempo_turnaround

	def __str__(self):
		return 'Processo: %02d = [Tamanho: %03d, Tempo de Espera: %03d, Tempo de TurnAround: %03d]' % (self.getId(), self.getTamanho(), self.getTempoEspera(), self.getTempoTurnaround())
