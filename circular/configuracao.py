# -*- encode : utf-8 -*-
from math import *

class Configuracao():
	processos = []

	def __init__(self, lista = [], quantum = 1, troca_contexto = 0):
		Configuracao.processos = lista
		self.quantum = quantum
		self.troca_contexto = troca_contexto

	def getLista(self):
		return Configuracao.processos

	def setLista(self, lista = []):
		Configuracao.processos = lista

	def getQuantum(self):
		return self.quantum

	def setQuantum(self, quantum):
		self.quantum = quantum

	def getTrocaContexto(self):
		return self.troca_contexto

	def setTrocaContexto(self, troca_contexto):
		self.troca_contexto = troca_contexto

	def enfileirar(self):
		if(len(Configuracao.processos) == 0):
			return 'Sem Processo'

		texto = ''
		for i in Configuracao.processos:
			texto = '%s%d\n' % (str(texto), i.getId())
		return texto

	def getQuantidadeAtividades(self):
		if(len(Configuracao.processos) == 0):
			return 'Sem Processo'

		texto = ''
		for i in Configuracao.processos:
			valor = ceil(i.getTamanho() / float(self.getQuantum()))
			texto = '%s%d - %d\n' % (str(texto), i.getId(), valor)
		return texto		

	def generateFila(self):
		if(len(Configuracao.processos) == 0):
			print 'Sem Processos'

		lista_temp, lista_esperas, index_espera, acumulador = [], [], 0, 0
		for i in Configuracao.processos:
			lista_temp.append([i.getId(), i.getTamanho()])
		#Lista Circular Funcionaria:
		while(len(lista_temp) > 0): #Enquanto Houver processos na lista;
			index = 0
			while(index < len(lista_temp)):
				if(lista_temp[index][1] > self.quantum):
					#print lista_temp[index][0], ' - ', self.quantum #Exibe a Retirada;
					acumulador += self.quantum
					lista_temp[index][1] -= self.quantum		
				else:
					#print lista_temp[index][0], ' - ', lista_temp[index][1] #Exibe a Retirada:
					acumulador += lista_temp[index][1]
					antigo = lista_temp.pop(index)
					lista_esperas.append([antigo[0], acumulador])
				#Incrementando a Troca de Contexto:
				index += 1
				acumulador += self.troca_contexto
			index = 0
		#Preenchendo os processos:
		lista_esperas.sort() #Ordena pelos primeiros indices;
		for i in range(len(lista_esperas)):
			Configuracao.processos[i].setTempoEspera(lista_esperas[i][1] - Configuracao.processos[i].getTamanho());
			Configuracao.processos[i].setTempoTurnaround(lista_esperas[i][1]);		

	def __str__(self):
		return 'Configuracao'
