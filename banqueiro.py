#Vetores:
recursos_existentes = [5,4,2,2]
recursos_disponiveis = [3,3,2,1]

#Matriz:
recursos_requisitados = [[2,0,3,1],[3,1,4,2],[0,0,0,0],[1,2,1,1]]

quantidade_estados_inseguros = 0
somatorio_requisicoes = 0
linhas_inseguras = []

for coluna in range(len(recursos_disponiveis)):
	somatorio_requisicoes += recursos_disponiveis[coluna]
	for linha in recursos_requisitados:
		somatorio_requisicoes += linha[coluna]
	
	if(somatorio_requisicoes > recursos_existentes[coluna]):
		linhas_inseguras.append(coluna)	
		quantidade_estados_inseguros += 1
	somatorio_requisicoes = 0
#Exibindo a Saida:
if(quantidade_estados_inseguros > 0):
	print "DeadLock"
	print "Linhas Inseguras: ",
	for i in linhas_inseguras:
		print i+1,
else:
	print "Nao Ha Deadlock"